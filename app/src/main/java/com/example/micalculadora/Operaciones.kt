package com.example.micalculadora

class Operaciones(var num1: Float, var num2: Float) {
    // var --> mutable
    public fun suma ():Float {
        return this.num1 + num2
    }
    public fun resta() : Float {
        return this.num1 - num2
    }
    public fun mult() : Float {
        return this.num1 * num2
    }
    public fun div() : Float {
        if(this.num1 != 0.0f && this.num2 != 0.0f)
            return this.num1 / num2
        else return 0.0f
    }
}